/* The height of the bar (in pixels) */
#define BAR_HEIGHT  18
/* Choose between an underline or an overline */
#define BAR_UNDERLINE 1
/* The thickness of the underline (in pixels). Set to 0 to disable. */
#define BAR_UNDERLINE_HEIGHT 1
/* Whether to put the bar at the screen bottom or not */
#define BAR_BOTTOM 1
/* The fonts used for the bar, comma separated. Only the first 2 will be used. */
#define BAR_FONT       "-*-terminus-medium-r-normal-*-12-*-*-*-c-*-*-1","fixed"
/* Color palette */
#define COLOR0    0x292929
#define COLOR1  0xBADA55
#define COLOR2    0x101010
#define COLOR3    0xcc6666
#define COLOR4    0xFB7D24
#define COLOR5    0xb3930f
#define COLOR6    0x286f8a
#define COLOR7    0x746d76
#define COLOR8    0x42ab9e
#define COLOR9    0xc0c0c0
