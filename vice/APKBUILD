# Maintainer: goodbox@udgb-team.com
pkgname=vice
pkgver=2.4
pkgrel=1
pkgdesc="The Versatile Commodore 8-bit Emulator"
url="http://vice-emu.sourceforge.net/"
arch="all"
license="GPL"
depends="bdftopcf mkfontdir mkfontscale"
depends_dev="alsa-lib-dev giflib-dev libxxf86vm-dev vte-dev libnet-dev libpcap-dev font-util-dev readline-dev libxt-dev libxmu-dev libxaw-dev gettext-dev readline-dev
util-macros"

makedepends="$depends_dev"
install=""
subpackages="$pkgname-doc"
source="http://downloads.sourceforge.net/project/vice-emu/releases/vice-$pkgver.tar.gz
	vice-2.4-x11video.patch
	vice-2.4-no-fc-cache-no-lib64.patch
	vice-2.4-notexi-notxt.patch
	vice-2.4-zlib-1.2.7.patch
	vice-2.4-giflib-5.1.0.patch
	vice-2.4-avoid_using_x86_ebx.patch"

_builddir="$srcdir"/vice-$pkgver
prepare() {
	local i
	cd "$_builddir"
	for i in $source; do
		case $i in
		*.patch) msg $i; patch -p1 -i "$srcdir"/$i || return 1;;
		esac
	done

	sed -i 's/AM_CONFIG_HEADER/AC_CONFIG_HEADERS/' configure.in
	mv configure.in configure.ac
	AT_NO_RECURSIVE=1 autoreconf -vi
}

build() {
	cd "$_builddir"
	export CC="ccache gcc"
	./configure \
		--with-x \
		--with-alsa \
		--enable-ethernet \
		--enable-fullscreen \
		--without-pulse \
		--without-oss \
		--disable-ffmpeg \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		|| return 1
	make || return 1
}

package() {
	cd "$_builddir"
	make DESTDIR="$pkgdir" install || return 1
	rm -f "$pkgdir"/usr/lib/*.la
}

md5sums="b017647a0c159bbe43cdb81762d1c577  vice-2.4.tar.gz
99931efcb779734c9df069e94f0b38a5  vice-2.4-x11video.patch
b0d1392664decd3169740baf90661710  vice-2.4-no-fc-cache-no-lib64.patch
877f93db1550ea81386aae7c3b578442  vice-2.4-notexi-notxt.patch
9d9f62f05a967a5926df496e86404148  vice-2.4-zlib-1.2.7.patch
94563e3023d804341f1d252c0e6d1384  vice-2.4-giflib-5.1.0.patch
0d0129655af2e64ec7cb91a2bcdcb1a2  vice-2.4-avoid_using_x86_ebx.patch"
sha256sums="ff8b8d5f0f497d1f8e75b95bbc4204993a789284a08a8a59ba727ad81dcace10  vice-2.4.tar.gz
affbf7a8b8d5c5d006a7cf7812385fff4a0300ca2adb991ad968b2bf333ead81  vice-2.4-x11video.patch
fd32383cbbd0a565d45751cdd0a174d521db9b7cab8e705aa1f74a892cd60576  vice-2.4-no-fc-cache-no-lib64.patch
cb24fbb9d2b1e6e30c17877c305d2e83cd7dd8c9355ca86b7415116a47e5471b  vice-2.4-notexi-notxt.patch
e64531926e678d9b8b193ef45551e104d309c25329ae29fe111bbac801a2551d  vice-2.4-zlib-1.2.7.patch
f59a8d55ac03e6c14466f80a1e6a5738b43a611365e124f4d70e7a3a58e28d56  vice-2.4-giflib-5.1.0.patch
e2304c7ed2f1ef9accb4486cd57c2c6bff8cd18d9d32dc06a3be0a62a2cedf4a  vice-2.4-avoid_using_x86_ebx.patch"
sha512sums="ad197fc35eb80c9738b7f4f551d350dbb4440c7c8103e5d472b7f7ea5106c1356e9d6e3f481552a028a313129ef233833d3147e03f6f00b9890229d5708e3ebb  vice-2.4.tar.gz
2ddaf4ccbeac96325e679aa7680b92aa0790e96e10b9a275a262aabbdf7ec640a763e89f51139e623a8a70b781254dd2c95c4854ba9cc340eae9cba89b36e839  vice-2.4-x11video.patch
d7641f6ccd631ea1279f31563dde64122fad5fa4883f26ead0b86d1cbd47f6f3d750bafc108d0cbd9dedc623f0e35d1f38449f958956e26bb03e086629887f49  vice-2.4-no-fc-cache-no-lib64.patch
6a410b338667821913adb6b75db0fdef93fb4792423f709d69308a60f29d0ea714f51461cd1b7c743f7abc8b2ad332e5799e6e171d063e868e326d9faa493cec  vice-2.4-notexi-notxt.patch
b3a2e8546a36cd189153b3f2c5a5884f95e5a89edeac4d1e632b215ce5262f6e2481d6c438f2b479ef51947426d124115094671c986f6ec232275c2c546abff2  vice-2.4-zlib-1.2.7.patch
6a7be998768fa85dbe6dc9c33f6429037e214e5a5d79a90b8b9c1670ba315a95af98c1c920baa54c64b63b2482d2a22e2c260ee0b3436bb5fe5c1f933683ec8c  vice-2.4-giflib-5.1.0.patch
4e8ad473046672c522a564be7ee84146fd15700856c79755e0b7b3f6880b597e7b46587838b5510a071e271dca1e74d845205caaaf1151c4015f5507065abb38  vice-2.4-avoid_using_x86_ebx.patch"
